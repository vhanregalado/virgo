<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends MY_Model {

    function __construct(){
        parent::__construct();
    }

    public function get_position_id_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT 
            p.id AS position_id 
        FROM
            users u 
        INNER JOIN POSITION p 
        WHERE 1 = 1 
        AND p.id = u.position_id
        AND u.id = '{$user_id}' 
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;

    }

    public function get_username_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT
            u.username AS username
        FROM
            users u
        WHERE 1 = 1
        AND u.id = '{$user_id}'
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;
    }

    public function get_user_basic_info_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT 
            u.firstname AS firstname,
            u.lastname AS lastname,
            ui.profile_pic AS profile_pic
        FROM
            users u 
        LEFT JOIN users_info ui 
            ON u.id = ui.users_id 
        WHERE 1 = 1 
        AND u.id = '{$user_id}' 
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;
    }


    public function get_users_info_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT             
            ui.job_title AS job_title,
            ui.address AS address,
            ui.contact_no AS contact_no,
            ui.profile_pic AS profile_pic 
        FROM
            users u 
        LEFT JOIN 
            users_info ui 
        ON 
            ui.users_id = u.id 
        WHERE 1 = 1 
        AND u.id = '{$user_id}'
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;

    }

    public function get_users_age_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT 
            TRUNCATE(
                (
                DATEDIFF(
                    CURRENT_DATE,
                    STR_TO_DATE(ui.birthdate, '%Y-%m-%d')
                ) / 365
                ),
                0
            ) AS age
            FROM
            users_info ui
        WHERE 1 = 1 
            AND ui.users_id = '{$user_id}'         
        ";

        $query = $this->query($query_string);

        $record = $query->result_array();

        return $record;

    }
    
    
}