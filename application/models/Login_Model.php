<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Model extends MY_Model {

    function __construct(){
        parent::__construct();
    }
    
    public function check_if_user_exit($username = ''){

        $return = array();

        $email = $this->db->escape_str($username);

        $query_string = "
        SELECT 
            u.username,
            u.email 
        FROM
            users u 
        WHERE 1 = 1 
        AND u.email = '{$email}' 
        ";
        
        $query = $this->db->query($query_string);

        if($query->num_rows() > 0){
            $return = $query->result_array();
        }

        return $return;

    }

    public function get_user_authentication($username, $password, $is_password_sha1=TRUE) {

        $record = array();
        
        // if($is_password_sha1 === TRUE){
        //     $sha1_password = sha1($password);
        // }
        // else{							
        //     $sha1_password = $password;
        // }

        $sha1_password = sha1($password);

        // $email = $this->db->escape_str($email);
        
        $query_string = "
        SELECT
            *
        FROM 
            users u
        WHERE 1 = 1
        AND u.username = '{$username}' 
        AND u.password = '{$sha1_password}'
        ";
        
        $query = $this->db->query($query_string);

        if($query->num_rows() > 0){
            $record = $query->row_array();
        }     
        
        return $record;

    }

	
}
