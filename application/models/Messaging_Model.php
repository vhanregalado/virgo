<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messaging_Model extends MY_Model {

    function __construct(){
        parent::__construct();
    }

    public function no_of_unread_messages_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT 
            COUNT(*) AS unread_msg 
        FROM
            messaging m 
        LEFT JOIN users r_user 
            ON r_user.id = m.recipient_id 
        WHERE 1 = 1 
        AND m.is_read = 0 
        AND r_user.id = '{$user_id}' 
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;

    }

    public function short_message_by_users_id($user_id = ''){

        $record = array();

        $query_string = "
        SELECT 
            m.id AS message_id,
            s_user.firstname AS s_firstname,
            s_user.lastname AS s_lastname,
            s_ui.profile_pic AS profile_pic,
            m.message_date AS message_date,
            m.message AS message 
        FROM
            messaging m 
        LEFT JOIN users s_user 
            ON s_user.id = m.sender_id 
        LEFT JOIN users r_user 
            ON r_user.id = m.recipient_id 
        LEFT JOIN users_info s_ui 
            ON s_ui.users_id = m.sender_id 
        WHERE 1 = 1 
            AND r_user.id = '{$user_id}' 
        ORDER BY m.message_date DESC 
        LIMIT 4 
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;

    }

    public function detailed_message_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT 
            m.id AS message_id,
            m.message_date AS message_date,
            m.message_title AS message_title,
            m.message AS message,
            CONCAT(
                s_user.firstname,
                ' ',
                s_user.lastname
            ) AS sender_name,
            m.is_read AS msg_status 
        FROM
            messaging m 
        LEFT JOIN users s_user 
            ON s_user.id = m.sender_id 
        WHERE 1 = 1 
            AND m.is_deleted = 0 
            AND m.recipient_id = '{$user_id}' 
        ORDER BY m.message_date DESC 
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;

    }

    public function message_table_by_users_id($user_id = ''){
        $record = array();

        $query_string = "
        SELECT 
        m.id AS message_id,
        m.message_date AS message_date,
        m.message_title AS message_title,
        (
            IF(
            LENGTH(m.message) > 90,
            CONCAT(SUBSTR(m.message, 1, 90),'...'),
            m.message
            )
        ) AS message,
        CONCAT(
            s_user.firstname,
            ' ',
            s_user.lastname
        ) AS sender_name,
        m.is_read AS msg_status 
        FROM
        messaging m 
        LEFT JOIN users s_user 
            ON s_user.id = m.sender_id 
        WHERE 1 = 1 
        AND m.is_deleted = 0 
        AND m.recipient_id = '{$user_id}' 
        ORDER BY m.message_date DESC  
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;

    }

    public function full_message_by_id($user_id = 0, $msg_id = 0){
        $record = array();

        $query_string = "
                SELECT 
        m.id AS message_id,
        m.message_date AS message_date,
        m.message_title AS message_title,
        m.message AS message,
        CONCAT(
            sender_u.firstname,
            ' ',
            sender_u.lastname
        ) AS sender_name,
        sender_u.email as email 
        FROM
        messaging m 
        LEFT JOIN users u 
            ON u.id = m.recipient_id 
        LEFT JOIN users sender_u 
            ON sender_u.id = m.sender_id 
        WHERE 1 = 1 
        AND u.id = '{$user_id}' 
        AND m.id = '{$msg_id}'
        ";

        $query = $this->db->query($query_string);

        $record = $query->result_array();

        return $record;

    }


    
}