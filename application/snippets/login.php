<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- important meta tag for responsive page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/production/images/nimbus_logo.png">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_old/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_old/bootstrap/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_old/fontawesome/css/all.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_old/css/base.css">
    
    <script src="<?php echo base_url(); ?>assets_old/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_old/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_old/bootstrap/js/bootstrap.js"></script>

    <script src="<?php echo base_url(); ?>assets_old/fontawesome/js/all.js"></script>

    <script src="<?php echo base_url(); ?>assets_old/js/login.js"></script>
    <script src="<?php echo base_url(); ?>assets_old/js/js-cookie.js"></script>

    <title>Virgo Project | Nimbuscore System</title>
    

</head>
<body>

    <div class="container-fluid">
         
        <div class="row justify-content-md-center">
            <div class="col col-lg-4">
                <div class="user-login">

                    <div class="jumbotron system-logo">
                        <h2><span><img src="<?php echo base_url()?>assets_old/images/nclogo.png" class="img-fluid img-logo"></img></span> Nimbuscore</h2>
                    </div>
                    
                    <div class="alert alert-danger alert-dismissible fade show message d-none" role="alert">                        
                        <strong><div class="header"></div></strong>
                        <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> -->
                    </div>

                    <?php if($referrer) : ?>

                    <div class="alert alert-warning alert-dismissible fade show error-session-expired" role="alert">
                        <strong>session expired</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <?php endif; ?>

                    <form class="login">

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user"></i></div>
                                </div>
                                <input type="text" name="username" class="form-control" placeholder="username">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-lock"></i></div>
                                </div>
                                <input type="password" name="password" class="form-control" placeholder="password">
                            </div>
                        </div>
                        
                        <div class="btn btn-primary btn-block login-btn"><i class="icon-login"></i> Login</div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        var sBaseURL = '<?php echo base_url(); ?>';       
    </script>

</body>
</html>