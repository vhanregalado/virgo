<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
    <nav>
        <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>

        <ul class="nav navbar-nav navbar-right">
        <li class="">
            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">            
            <?php foreach($user as $user_key => $users) :?>

                  <?php if($users['profile_pic'] != '') : ?>
                    <img src="<?php echo base_url()?>assets/profile_img/<?php echo $users['profile_pic'] ?>" alt="..." >
                  <?php endif; ?>

                  <?php if($users['profile_pic'] == '') : ?>
                    <img src="<?php echo base_url()?>assets/profile_img/user.png" alt="..." >
                  <?php endif; ?>

                <?php endforeach; ?>
            <?php
            foreach($user as $user_key => $users){
                echo $users['firstname']." ".$users['lastname'];
            }
            ?>
            <span class=" fa fa-angle-down"></span>
            </a>
            <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="#"> Profile</a></li>            
            <li><a class="logout" href="#"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
            </ul>
        </li>

        <li role="presentation" class="dropdown">
            <a href="" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-envelope-o"></i>

            <!-- short message function -->
            
            <?php foreach($unread_message as $unread_message_key => $unread_messages) : ?>
            
                <?php if($unread_messages['unread_msg'] != 0) : ?>
                    <span class="badge bg-green">
                        <?php echo $unread_messages['unread_msg']; ?>
                    </span>
                <?php endif; ?>

            <?php endforeach; ?>
            
            </a>
            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
            <?php foreach($short_message as $short_message_key => $short_messages) : ?>

                <li>
                    <a>
                    <span class="image"><img src="<?php echo base_url()?>assets/profile_img/<?php echo $short_messages['profile_pic'] ?>" alt="Profile Image" /></span>
                    <span>
                        <span><?php echo $short_messages['s_firstname']." ".$short_messages['s_lastname'] ?></span>
                        <span class="time">
                            <?php 
                            echo time_elapsed_string($short_messages['message_date']);
                            ?>
                        </span>
                    </span>
                    <span class="message">
                        <?php echo strlen($short_messages['message']) > 90 ? substr($short_messages['message'],0,90)."..." : $short_messages['message'];?>
                    </span>
                    </a>
                </li>

            <?php endforeach; ?>

            <?php foreach($unread_message as $unread_message_key => $unread_messages) : ?>

                <?php if($unread_messages['unread_msg'] > 4) : ?>

                <li>
                    <div class="text-center">
                    <a href="<?php echo base_url()?>message">
                        <strong>See All Messages</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    </div>
                </li>

                <?php endif; ?>

                <?php if($unread_messages['unread_msg'] == 0) : ?>

                <li>
                    <div class="text-center">
                    <a>
                        <strong>No New Messages</strong>                        
                    </a>
                    </div>
                </li>

                <?php endif; ?>
            
            <?php endforeach; ?>
            
            </ul>
        </li>
        </ul>
    </nav>
    </div>
</div>
<!-- /top navigation -->