<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="User Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Help">
        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Messages" href="<?php echo base_url()?>message">
        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
    </a>
    <a class="logout" data-toggle="tooltip" data-placement="top" title="Logout" href="#">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
</div>