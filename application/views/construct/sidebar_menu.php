<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="<?php echo base_url()?>dashboard">Dashboard</a></li>
                <li><a href="<?php echo base_url()?>message">Messages</a></li>              
            </ul>
            </li>
            <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="#">User Settings</a></li>
                <li><a href="#">Link 02</a></li>
                <li><a href="#">Link 03</a></li>
                <li><a href="#">Link 04</a></li>
                <li><a href="#">Link 05</a></li>
                <li><a href="#">Link 06</a></li>
            </ul>
            </li>
            <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="#">Link 01</a></li>
                <li><a href="#">Link 02</a></li>
                <li><a href="#">Link 03</a></li>
                <li><a href="#">Link 04</a></li>
                <li><a href="#">Link 05</a></li>
                <li><a href="#">Link 06</a></li>
            </ul>
            </li>
            <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="#">Link 01</a></li>
                <li><a href="#">Link 02</a></li>
                <li><a href="#">Link 03</a></li>
                <li><a href="#">Link 04</a></li>
                <li><a href="#">Link 05</a></li>
                <li><a href="#">Link 06</a></li>
            </ul>
            </li>
            <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="#">Link 01</a></li>
                <li><a href="#">Link 02</a></li>
                <li><a href="#">Link 03</a></li>
                <li><a href="#">Link 04</a></li>
                <li><a href="#">Link 05</a></li>
                <li><a href="#">Link 06</a></li>
            </ul>
            </li>
            <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="#">Link 01</a></li>
                <li><a href="#">Link 02</a></li>
                <li><a href="#">Link 03</a></li>
                <li><a href="#">Link 04</a></li>
                <li><a href="#">Link 05</a></li>
                <li><a href="#">Link 06</a></li>
            </ul>
            </li>
        </ul>
    </div>   

</div>