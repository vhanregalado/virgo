<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <link rel="icon" href="<?php echo base_url()?>assets/production/images/nimbus_logo.png" type="image/ico"/>

    <title>Nimbuscore | <?php echo $page_title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url()?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url()?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url()?>assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url()?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url()?>assets/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url()?>assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Sweetalert2 plugins -->
    <link href="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="<?php echo base_url()?>assets/build/css/custom.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/build/css/main.css" rel="stylesheet">

    <!-- Custom CSS Plugins Files-->

    <?php if(isset($css) AND array_check($css)): ?>
      <?php foreach ($css as $css_value) : ?>
        <link href="<?php echo base_url() ."assets/".$css_value .".css" ?>" rel="stylesheet" type="text/css" />
      <?php endforeach;?>
	  <?php endif;?>  
    

  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">            
              <a href="dashboard" class="site_title"><img src="<?php echo base_url()?>assets/production/images/nimbus_logo.png" alt="" class="img-circle img-thumbnail" style="width:40px;height:auto;"> 
              <span>Nimbuscore</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">

                <?php foreach($user as $user_key => $users) :?>

                  <?php if($users['profile_pic'] != '') : ?>
                    <img src="<?php echo base_url()?>assets/profile_img/<?php echo $users['profile_pic'] ?>" alt="..." class="img-circle profile_img">
                  <?php endif; ?>

                  <?php if($users['profile_pic'] == '') : ?>
                    <img src="<?php echo base_url()?>assets/profile_img/user.png" alt="..." class="img-circle profile_img">
                  <?php endif; ?>

                <?php endforeach; ?>
                
              </div>
              <div class="profile_info">
                <span>Welcome</span>
                <h2>
                  <?php
                      foreach($user as $user_key => $users){
                        echo $users['firstname']." ".$users['lastname'];
                      }                    
                  ?>
                </h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php $this->load->view('construct/sidebar_menu'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->load->view('construct/menu_footer_buttons'); ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php $this->load->view('construct/top_navigation'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <?php echo $content ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          <!-- <h1><img src="<?php echo base_url()?>assets/production/images/nimbus_logo.png" alt="" class="img-circle img-thumbnail" style="width:40px;height:auto;"> Nimbuscore</h1> -->
                  <p>©<?php echo $copyright ?> All Rights Reserved. Nimbuscore Systems is created by G. L. Regalado</p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url()?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url()?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url()?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url()?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url()?>assets/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url()?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url()?>assets/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url()?>assets/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url()?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url()?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url()?>assets/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url()?>assets/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url()?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Sweetalert2 plugins -->
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.all.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.all.min.js"></script>

    <!-- Custom Theme Scripts --> 
    <script src="<?php echo base_url()?>assets/build/js/custom.min.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/main.js"></script>

    <script>
      var sBaseURL = "<?php echo base_url(); ?>";
    </script>

    
    
    
    <!-- Custom Javascript Plugin Files -->
    
    <?php if(isset($javascripts) AND array_check($javascripts)): ?>
      <?php foreach ($javascripts as $javascript) : ?>
        <script type="text/javascript" src="<?php echo base_url() ."assets/".$javascript .".js" ?>"></script>
      <?php endforeach;?>
	  <?php endif;?>

	
  </body>
</html>
