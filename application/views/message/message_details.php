<div class="message-details">

    <?php foreach($message_detail as $message_detail_key => $message_details) :?>

    <div class="page-title">
        <div class="title_left">
            <h3>Messages</h3>
        </div>

        <!-- <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
            </div>
        </div> -->
    </div>

    <div class="clearfix"></div>

    <div class="row">
    <div class="col-md-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Message Details</h2>
            <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <!-- CONTENT MAIL -->
            <div class="col-sm-12">
                <div class="inbox-body">
                <div class="mail_heading row">
                    <!-- <div class="col-md-8">
                        <div class="btn-group">
                            <button class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Reply</button>
                            <button class="btn btn-sm btn-default" type="button"  data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i></button>
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></button>
                            <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                        </div>
                    </div> -->
                    <div class="col-md-12 text-right">
                    <p class="date"><?php echo $message_details['message_date']; ?></p>
                    </div>
                    <div class="col-md-12">
                    <h4><?php echo $message_details['message_title']; ?></h4>
                    </div>
                </div>
                <div class="sender-info">
                    <div class="row">
                    <div class="col-md-12">
                        <strong><?php echo $message_details['sender_name']; ?></strong>
                        <span>(<?php echo $message_details['email']; ?>)</span> to
                        <strong>me</strong>
                        <a class="sender-dropdown"><i class="fa fa-chevron-down"></i></a>
                    </div>
                    </div>
                </div>
                <div class="view-mail">
                    <p><?php echo $message_details['message']; ?></p>
                </div>
                
                <div class="btn-group">
                    <button class="btn btn-sm btn-primary reply-btn" type="button"><i class="fa fa-reply"></i> Reply</button>
                    <button class="btn btn-sm btn-default" type="button"  data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i></button>
                    <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></button>
                    <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                </div>
                </div>

            </div>
            <!-- /CONTENT MAIL -->
            </div>
        </div>
        </div>
    </div>
    </div>

    <?php endforeach; ?>

</div>