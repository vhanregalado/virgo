<div class>
    <div class="page-title">
        <div class="title_left">
            <h3>Messages</h3>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
    <div class="x_title">
    <h2>Message List<small>Users</small></h2>
    <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
        </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
    </ul>
    <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <p class="text-muted font-13 m-b-30">
        web messaging module
    </p>
    
    <table class="table table-striped table-hover nowrap message-table" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>message id</th>
            <th>message date</th>
            <th>message title</th>
            <th>message</th>
            <th>sender</th>            
        </tr>
        </thead>
        <tbody>
        <?php foreach( $detailed_message as $detailed_message_key => $detailed_messages ) : ?>
        <tr>            
            <td><?php echo $detailed_messages['message_id']?></td>
            <td><?php echo $detailed_messages['message_date']?></td>
            <td><?php echo $detailed_messages['message_title']?></td>
            <td><?php echo strlen($detailed_messages['message']) > 90 ? substr($detailed_messages['message'],0,90)."..." : $detailed_messages['message']?></td>
            <td><?php echo $detailed_messages['sender_name']?></td>
        </tr>       
        <?php endforeach; ?>        
        </tbody>
    </table>
    
    
    </div>
</div>
</div>