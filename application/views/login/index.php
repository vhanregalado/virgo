<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo base_url()?>assets/production/images/nimbus_logo.png" type="image/ico"/>

    <title>Nimbuscore | Login</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url()?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url()?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url()?>assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Sweetalert2 plugins -->
    <link href="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url()?>assets/build/css/custom.min.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>    

    <script src="<?php echo base_url(); ?>assets/build/js/login.js"></script>

    <script src="<?php echo base_url(); ?>assets/build/js/js-cookie.js"></script>


    <!-- Sweetalert2 plugins -->
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.all.js"></script>
    <script src="<?php echo base_url()?>assets/vendors/sweetalert2/sweetalert2.all.min.js"></script>

    <script>
        var sBaseURL = '<?php echo base_url() ?>';
    </script>


  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form class="login">
              <h1>Login Form</h1>

                <div class="alert alert-danger alert-dismissible message" style="display:none;"role="alert">                        
                    <strong><div class="header"></div></strong>
                </div>

                <?php if($referrer) : ?>
                    <div class="alert alert-warning alert-dismissible" role="alert"> 
                        session expired
                    </div>
                <?php endif; ?>

              <div>
                <input name="username" type="text" class="form-control" placeholder="Username" />
              </div>
              <div>
                <input name="password" type="password" class="form-control" placeholder="Password" />
              </div>
              <div>
                <div class="btn btn-warning btn-block login-btn"><i class="icon-login"></i> Log in</div>
                <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="<?php echo base_url()?>assets/production/images/nimbus_logo.png" alt="" class="img-circle img-thumbnail" style="width:40px;height:auto;"> Nimbuscore</h1>
                  <p>©<?php echo $copyright ?> All Rights Reserved. Nimbuscore Systems is created by G. L. Regalado</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <script>
            var sBaseURL = '<?php echo base_url(); ?>';
        </script>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input name="username" type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input name="email" type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input name="password" type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <input name="confirm_password" type="password" class="form-control" placeholder="Confirm Password" required="" />
              </div>
              <div class="separator"><p>User Information</p></div>
              <div>
                <input name="firstname" type="text" class="form-control" placeholder="Firstname" required="" />
              </div>
              <div>
                <input name="middlename" type="text" class="form-control" placeholder="Middlename" required="" />
              </div>
              <div>
                <input name="lastname" type="text" class="form-control" placeholder="Lastname" required="" />
              </div>
              
              <div class="btn btn-default btn-block register-btn">Register</div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="<?php echo base_url()?>assets/production/images/nimbus_logo.png" alt="" class="img-circle img-thumbnail" style="width:40px;height:auto;"> Nimbuscore</h1>
                  <p>©<?php echo $copyright ?> All Rights Reserved. Nimbuscore Systems is created by G. L. Regalado</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>

    
  </body>
</html>
