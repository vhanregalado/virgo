<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    function __construct(){
        parent::__construct();
		parent::_verify_user_authentication();		
    }
	
	public function index(){
        $data = array();

		$user_id = $this->session->userdata('user_id');		
		
		$options = array(
			'page'	 		=>	'dashboard/index',
			'params'	 	=>	$data,
			'page_title'	=>	'Dashboard',
			'main_page'		=>	'dashboard'
		);

		$this->render_page($options);
	}

}
