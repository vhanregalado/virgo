<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Login_Model');
	}

	public function index(){

		$data = array();

		$data['copyright'] = date('Y');

		$data['referrer'] = 0;

		if($this->session->userdata('user_id')){
			$data['referrer'] = 1;
		}
		
		$this->load->view('login/index',$data);

	}

	/**
	* This will be the process of login to check if there is error
	*/

	public function check_user_login(){
		if($this->input->post()){

			$return = array();
			$return['success'] = true;
			$return['message'] = '';
			$return['referrer'] = '';
			
			$user_check = $this->validate_login($this->input->post('username'), $this->input->post('password'));

			if(count($user_check['errors'])>0){
				$return['success'] = false;
				$return['message'] = implode('<br />',$user_check['errors']);
			}

			if(isset($user_check['user']['referrer']) AND $user_check['user']['referrer'] != ''){
				$return['referrer'] = $user_check['user']['referrer'];
			}

			echo json_encode($return);			

		}else{
			show_404();
		}
				
	}

	/**
	* This is to validate the email and password if correct
	* @param 	$email 		string		email/username of the user
	* @param 	$password 	string		password of the user
	* @return 				array 		errors
	*/

	private function validate_login($username, $password){

		$errors = $user = array();

		if(trim($username) == '') 	$errors[] = 'Please enter your username.';
		if(trim($password) == '') 	$errors[] = 'Please enter your password.';
			
		if(count($errors)==0){
			
			$user = $this->Login_Model->get_user_authentication($username, $password);
			
			if(array_check($user)){
				
				if($user['status'] == 0){
					$errors[] = "Your account is disabled.";
				}
				else{
					$this->session->set_userdata('user_id', $user['id']);

					$referer = $this->session->userdata('referrer');
						
					if(!empty($referrer)) {
						$this->session->unset_userdata('referrer');
						$user['referer'] = $referrer;
					} 
				}
			}
			else{
				$errors[] = 'incorrect username / password.';
			}
			
		}
		
		return array('errors'=>$errors, 'user'=>$user);	
	}

	public function logout(){
		$this->session->unset_userdata('user_id');
		redirect(base_url());
	}

}
