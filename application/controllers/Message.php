<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends MY_Controller {

	function __construct(){
        parent::__construct();
		parent::_verify_user_authentication();		
    }
	
	public function index(){

        $data = array();

		$user_id = $this->session->userdata('user_id');

		$data['detailed_message'] = $this->Messaging->detailed_message_by_users_id($user_id);
		
		$data['css'] = array(
			'vendors/datatables.net-bs/css/dataTables.bootstrap.min',
			'vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min',
			'vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min',
			'vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min',
			'vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min'
		);

		$data['javascripts'] = array(
			'vendors/datatables.net/js/jquery.dataTables.min',
			'vendors/datatables.net-bs/js/dataTables.bootstrap.min',
			'vendors/datatables.net-buttons/js/dataTables.buttons.min',
			'vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min',
			'vendors/datatables.net-buttons/js/buttons.flash.min',
			'vendors/datatables.net-buttons/js/buttons.html5.min',
			'vendors/datatables.net-buttons/js/buttons.print.min',
			'vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min',
			'vendors/datatables.net-keytable/js/dataTables.keyTable.min',
			'vendors/datatables.net-responsive/js/dataTables.responsive.min',
			'vendors/datatables.net-responsive-bs/js/responsive.bootstrap',
			'vendors/datatables.net-scroller/js/dataTables.scroller.min',
			'vendors/jszip/dist/jszip.min',
			'vendors/pdfmake/build/pdfmake.min',
			'vendors/pdfmake/build/vfs_fonts',
			'build/js/message'
		);

		$options = array(
			'page'	 		=>	'message/index',
			'params'	 	=>	$data,
			'page_title'	=>	'Message',
			'main_page'		=>	'Message'
		);

		$this->render_page($options);		
	}

	public function message_table(){
		$message_table = array();
		$user_id = $this->session->userdata('user_id');
		$message_table = $this->Messaging->message_table_by_users_id($user_id);
		echo json_encode($message_table);
	}

	public function message_details($id = 0){
		$data = array();
		$user_id = $this->session->userdata('user_id');
		$data['message_detail']  = $this->Messaging->full_message_by_id($user_id, $id);

		$data['javascripts'] = array(			
			'build/js/message_detail'
		);


		$options = array(
			'page'			=>	'message/message_details',
			'params'		=>	$data,
			'page_title' 	=>	'Message',
			'main_page'		=>	'Message'
		);

		
		$this->render_page($options);	
		
	}


}
