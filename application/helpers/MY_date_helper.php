<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('time_elapsed_string')){

    function time_elapsed_string($datetime, $full = false) {

        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );

        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}

if ( ! function_exists('time_elapsed_string_mini')){

    function time_elapsed_string_mini($datetime, $full = false) {

        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'y',
            'm' => 'm',
            'w' => 'w',
            'd' => 'd',
            'h' => 'h',
            'i' => 'min',
            's' => 's'
        );

        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(',', $string) : implode(',', $string);
    }
}

if ( ! function_exists('local_datetime')){

    function local_datetime($date = '', $is_strtotime = FALSE){

        $timezone = 'UP8'; //philippines

        if($date == '0000-00-00 00:00:00'){
            return strtotime('0000-00-00 00:00:00');
        }
        elseif($date == ''){
            $date = date('Y-m-d H:i:s',now());
        }

        $timestamp = strtotime($date);
        $daylight_saving = FALSE;

        $return_date = gmt_to_local($timestamp, $timezone, $daylight_saving);

        $return_date = date('Y-m-d H:i:s', $return_date);

        if($is_strtotime){
            $return_date = strtotime($return_date);
        }

        return $return_date;
    }
}

if ( ! function_exists('utc_datetime')){

    function utc_datetime(){

        $date_time 		= new DateTime;
        $timestamp 		= $date_time->getTimestamp();
        $return_date 	= date('Y-m-d H:i:s', $timestamp);

        return $return_date;
    }
}

if ( ! function_exists('convert_to_local')){

    function convert_to_local($date_time = '00-00-000 00:00:00', $format = 'Y-m-d H:i:s', $timezone = "Asia/Manila"){

        $return_date = new DateTime($date_time);
        $return_date->setTimezone(new DateTimeZone($timezone));
        $return_date = $return_date->format($format);
        
        return $return_date;
    }
}

if ( ! function_exists('check_month')){

    function check_month($month){
        $return = '';

        if($month == date('m', strtotime(local_datetime()))){
            $return = 'selected';
        }

        return $return;
    }
}
