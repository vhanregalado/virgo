<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('kprint')){
    function kprint($variable){
        echo "<pre>";
        print_r($variable);
        echo "</pre>";
    }
}

if ( ! function_exists('array_check')){
    function array_check($array){
        if(is_array($array) and count($array) > 0){
            return true;
        }
        else{
            return false;
        }
    }
}

