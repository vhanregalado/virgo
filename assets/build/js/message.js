$(document).ready(function(){   

    var table = $('.message-table').DataTable({
        "ordering" : false
        // ajax: {
        //     url: sBaseURL + '/message/message_table',
        //     dataSrc: ''
        // },
        // columns: [
        //     {data: 'message_id'},
        //     {data: 'message_date'},
        //     {data: 'message_title'},
        //     {data: 'message'},
        //     {data: 'sender_name'}
        // ],
        // "columnDefs":[{
        //     "targets": 0,
        //     "data": null,
        //     "render": function(data, type, row){
        //         return '<a href="'+ sBaseURL+'messages/message_detail/' + data + '">'+data+'</a>';
        //     }
        // }]
    });   
   

    $('.message-table tbody').on('click', 'tr', function(){
        var data = table.row(this).data();
        // swal(data[2],data[3])
        window.location.href=sBaseURL + 'message/message_details/' + data[0];
    });   
    
})