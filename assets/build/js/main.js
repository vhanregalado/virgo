$(document).ready(function(){
    $('.logout').click(function(){
        swal({
            title: 'Log Out',
            text: "do you want to logout?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'yes'
          }).then((result) => {
              if(result.value){
                window.location.replace( sBaseURL + 'login/logout');
              }
          })     
    })
    
})