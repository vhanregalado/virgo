$(document).ready(function(){
    
    $('body').on('keypress', function(e){
        if(e.which == 13){            
            login($('.login-btn'));
        }
    });

    $('.login-btn').click(function(e){
        e.preventDefault();
        login($(this));
        return false;        
    });

    // $('.message .close').on('click', function(){
    //     $(this).closest('.message').transition('fade');
    // });

    // $('.login .negative.message').hide();

    function login(button){       

        var data = {};

        data.username = $('form.login input[name="username"]').val();
        data.password = $('form.login input[name="password"]').val();
        data.nimbuscore_token = Cookies.set('nimbuscore_cookie');

        $.ajax({
            url :'login/check_user_login',
            type : 'post',
            dataType : 'json',
            data : data,
            beforeSend : function(){
                $('.login-btn').addClass('disabled');       
                $('.icon-login').addClass('fa fa-spinner fa-spin');
            },
            success : function(oData){
                if(oData.success){
                    if(oData.referrer != ''){
                        window.location = oData.referrer;
                    }else{
                        window.location = sBaseURL + "dashboard";
                    }

                }else{
                    $('form.login .message').removeClass('d-none');
                    $('form.login .message .header').html(oData.message);
                    $('form.login .message').show();
                }

            },
            error : function(){
                swal({type:'error',text:'An error occured. Please contact your administrator.'})
            },
            complete : function(){               
                $('.login-btn').removeClass('disabled');       
                $('.login-btn .icon-login').removeClass('fa fa-spinner fa-spin');
            }
        });

    }
});
